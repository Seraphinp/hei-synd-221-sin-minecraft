package ch.hevs.isi.smartcontrol;


import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.Timer;
import java.util.TimerTask;

public class SmartControl {
    private static SmartControl sc = new SmartControl();
    private SmartControl(){
    }


    public static SmartControl getInstance(){
        return sc;
    }

    /** Timer for doControl()
     *
     * @param time time delay in ms
     */
    public void smartControl(int time){
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    doControl();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },time,time);

    }


    /** Manage the electrical system
     *
     * @throws Exception
     */
    public void doControl() throws Exception {
        FloatDataPoint GRID_U_FLOAT = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("GRID_U_FLOAT");
        FloatDataPoint BATT_P_FLOAT = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("BATT_P_FLOAT");
        FloatDataPoint BATT_CHRG_FLOAT = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
        FloatDataPoint SOLAR_P_FLOAT = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("SOLAR_P_FLOAT");
        FloatDataPoint WIND_P_FLOAT = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("WIND_P_FLOAT");
        FloatDataPoint COAL_P_FLOAT = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("COAL_P_FLOAT");
        FloatDataPoint COAL_AMOUNT  = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("COAL_AMOUNT");
        FloatDataPoint HOME_P_FLOAT = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("HOME_P_FLOAT");
        FloatDataPoint PUBLIC_P_FLOAT  = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("PUBLIC_P_FLOAT");
        FloatDataPoint FACTORY_P_FLOAT = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("FACTORY_P_FLOAT");
        FloatDataPoint BUNKER_P_FLOAT  = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("BUNKER_P_FLOAT");
        FloatDataPoint WIND_FLOAT  = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("WIND_FLOAT");
        FloatDataPoint WEATHER_FLOAT = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("WEATHER_FLOAT");
        FloatDataPoint WEATHER_FORECAST_FLOAT  = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("WEATHER_FORECAST_FLOAT");
        FloatDataPoint WEATHER_COUNTDOWN_FLOAT  = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("WEATHER_COUNTDOWN_FLOAT");
        FloatDataPoint CLOCK_FLOAT  = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("CLOCK_FLOAT");
        FloatDataPoint REMOTE_COAL_SP = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
        FloatDataPoint REMOTE_FACTORY_SP  = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
        BinaryDataPoint REMOTE_SOLAR_SW = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
        BinaryDataPoint REMOTE_WIND_SW  = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_WIND_SW");
        FloatDataPoint FACTORY_ENERGY  = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("FACTORY_ENERGY");
        FloatDataPoint SCORE  = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("SCORE");
        FloatDataPoint COAL_SP = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("COAL_SP");
        FloatDataPoint FACTORY_SP = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("FACTORY_SP");



        // Energy consumed
        float conso = SOLAR_P_FLOAT.getValue() + WIND_P_FLOAT.getValue() + COAL_P_FLOAT.getValue() - HOME_P_FLOAT.getValue()
                - PUBLIC_P_FLOAT.getValue() - FACTORY_P_FLOAT.getValue() - BUNKER_P_FLOAT.getValue();

        System.out.println("Consommmation : " + conso);

        // Connect solar panels and wind turbine
       /* REMOTE_SOLAR_SW.setValue(true);
        REMOTE_WIND_SW.setValue(true);*/

        // Test if night
        if(CLOCK_FLOAT.getValue() < 0.25f || CLOCK_FLOAT.getValue() > 0.75f) {
            REMOTE_FACTORY_SP.setValue(0f); // Factory is stopped

            if (BATT_CHRG_FLOAT.getValue() < 0.5f || GRID_U_FLOAT.getValue() < 730f) {
                REMOTE_COAL_SP.setValue(1f); // Start coal factory
                REMOTE_WIND_SW.setValue(true);
                REMOTE_SOLAR_SW.setValue(true);
            } else {
                if (BATT_CHRG_FLOAT.getValue() < 0.8f && GRID_U_FLOAT.getValue() < 900) {
                    REMOTE_WIND_SW.setValue(true);
                    REMOTE_SOLAR_SW.setValue(true);
                    if (conso < 0f) {
                        REMOTE_COAL_SP.setValue(-conso); // Use the production of coal
                    } else {
                        REMOTE_COAL_SP.setValue(0f);
                    }
                }else{
                    REMOTE_COAL_SP.setValue(0f);
                    REMOTE_WIND_SW.setValue(false);
                }
            }


        }else{ // Day
            REMOTE_FACTORY_SP.setValue(1f); // Factory is started

            if (BATT_CHRG_FLOAT.getValue() < 0.5f || GRID_U_FLOAT.getValue() < 730f) {
                REMOTE_COAL_SP.setValue(1f); // Start coal factory
                REMOTE_WIND_SW.setValue(true);
                REMOTE_SOLAR_SW.setValue(true);
            } else {
                if (BATT_CHRG_FLOAT.getValue() < 0.95f && GRID_U_FLOAT.getValue() < 940f) {
                    REMOTE_WIND_SW.setValue(true);
                    REMOTE_SOLAR_SW.setValue(true);
                    if (conso < 0f) {
                        REMOTE_COAL_SP.setValue(-conso); // Use the production of coal
                    } else {
                        REMOTE_COAL_SP.setValue(0f);
                    }
                }else{
                    REMOTE_COAL_SP.setValue(0f);
                    REMOTE_WIND_SW.setValue(false);
                    REMOTE_SOLAR_SW.setValue(false);
                }
            }

        }

        System.out.println("Score : " + SCORE.getValue());

    }
    public static void main(String[] args){
        sc.smartControl(10000);

    }


}
