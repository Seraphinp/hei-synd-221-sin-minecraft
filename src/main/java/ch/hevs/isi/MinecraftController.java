package ch.hevs.isi;

import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.field.ModbusAccessor;
import ch.hevs.isi.smartcontrol.SmartControl;
import ch.hevs.isi.utils.Utility;
import ch.hevs.isi.web.WebConnector;

public class MinecraftController {
    public static boolean USE_MODBUS4J = false;

    public static void usage() {
        System.out.println("Parameters: <InfluxDB Server> <Group Name> <MordbusTCP Server> <modbus TCP port> [-modbus4j]");
        System.exit(1);
    }

    public static void main(String[] args) {

        String dbHostName    = "localhost";
        String dbName        = "SIn15";
        String dbUserName    = "SIn15";
        String dbPassword    = "229a29f44e110c7299bf460c056ff372";

        String modbusTcpHost = "localhost";
        int    modbusTcpPort = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length == 4 || args.length == 5) {
            parameters = args;

            // Decode parameters for influxDB
            dbHostName  = parameters[0];
            dbUserName  = parameters[1];
            dbName      = dbUserName;
            dbPassword  = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[2];
            modbusTcpPort = Integer.parseInt(parameters[3]);

            if (args.length == 5) {
                USE_MODBUS4J = (parameters[4].compareToIgnoreCase("-modbus4j") == 0);
            }
        } else {
            usage();
        }


        FieldConnector.getInstance().adBooleanRegister(609, "SOLAR_CONNECT_SW", true);
        FieldConnector.getInstance().adFloatRegister(49, "BATT_CHRG_FLOAT", false,1,0);
        FieldConnector.getInstance().adFloatRegister(57, "BATT_P_FLOAT", false,6000,-3000);
        FieldConnector.getInstance().adFloatRegister(89, "GRID_U_FLOAT", false, 0, 1000);
        FieldConnector.getInstance().adFloatRegister(57, "BATT_P_FLOAT", false, -3000, 6000);
        FieldConnector.getInstance().adFloatRegister(49, "BATT_CHRG_FLOAT", false, 0, 1);
        FieldConnector.getInstance().adFloatRegister(61, "SOLAR_P_FLOAT", false, 0, 1500);
        FieldConnector.getInstance().adFloatRegister(53, "WIND_P_FLOAT", false, 0, 1000);
        FieldConnector.getInstance().adFloatRegister(81, "COAL_P_FLOAT", false, 0, 600);
        FieldConnector.getInstance().adFloatRegister(65, "COAL_AMOUNT", false, 0, 1);
        FieldConnector.getInstance().adFloatRegister(101, "HOME_P_FLOAT", false, 0, 1000);
        FieldConnector.getInstance().adFloatRegister(97, "PUBLIC_P_FLOAT", false, 0, 500);
        FieldConnector.getInstance().adFloatRegister(105, "FACTORY_P_FLOAT", false, 0, 2000);
        FieldConnector.getInstance().adFloatRegister(93, "BUNKER_P_FLOAT", false, 0, 500);
        FieldConnector.getInstance().adFloatRegister(301, "WIND_P_FLOAT", false, 0, 1);
        FieldConnector.getInstance().adFloatRegister(305, "WEATHER_FLOAT", false, 0, 1);
        FieldConnector.getInstance().adFloatRegister(309, "WEATHER_FORECAST_FLOAT", false, 0, 1);
        FieldConnector.getInstance().adFloatRegister(313, "WEATHER_COUNTDOWN_FLOAT", false, 0, 600);
        FieldConnector.getInstance().adFloatRegister(317, "CLOCK_FLOAT", false, 0, 1);
        FieldConnector.getInstance().adFloatRegister(209, "REMOTE_COAL_SP", false, 0, 1);
        FieldConnector.getInstance().adFloatRegister(205, "REMOTE_FACTORY_SP", false, 0, 1);
        FieldConnector.getInstance().adBooleanRegister(401, "REMOTE_SOLAR_SW", true);
        FieldConnector.getInstance().adBooleanRegister(405, "REMOTE_WIND_SW", true);
        FieldConnector.getInstance().adFloatRegister(341, "FACTORY_ENERGY", false, 0, 3600000);
        FieldConnector.getInstance().adFloatRegister(345, "SCORE", false, 0, 3600000);
        FieldConnector.getInstance().adFloatRegister(601, "COAL_SP", false, 0, 1);
        FieldConnector.getInstance().adFloatRegister(605, "FACTORY_SP", false, 0, 1);
        FieldConnector.getInstance().adBooleanRegister(609, "SOLAR_CONNECT_SW", false);
        FieldConnector.getInstance().adBooleanRegister(613, "WIND_CONNECT_SW", false);




        FieldConnector.getInstance().startPollIn(5000);


        SmartControl.getInstance().smartControl(10000);

    }
}
