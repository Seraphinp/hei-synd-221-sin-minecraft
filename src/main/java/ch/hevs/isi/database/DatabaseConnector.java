package ch.hevs.isi.database;
import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;


public class DatabaseConnector implements DataPointListener {

    private static DatabaseConnector dbc = null;

    String url = "https://influx.sdi.hevs.ch/write?db=SIn15";
    String userpass = "SIn15:229a29f44e110c7299bf460c056ff372";
    String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());
    String type = "binary/octet-stream";


    private DatabaseConnector(){
    }

    /**
     * @return DatabaseConnector
     */
    public static DatabaseConnector getInstance(){
        if(dbc == null){
            dbc = new DatabaseConnector();
        }
        return dbc;
    }

    /** Sends an HTTP POST to the database
     *
     * @param label the name of the DataPoint
     * @param value the value of the DataPoint
     */
    private void pushToDataBaseConnector(String label, String value){
        try {
            URL url = new URL(dbc.url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            connection.setRequestProperty("Content-Type", type);
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(label + " value=" + value);
            writer.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            if (connection.getResponseCode() == 204) {
                while ((in.readLine()) != null) {
                }
            }

            in.close();
            writer.close();
            connection.disconnect();

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Print and push a FloatDataPoint on the DataBase
     * @param fdp FloatDataPoint
     */
    @Override
    public void onNewValue(FloatDataPoint fdp){
        System.out.println(fdp.getLabel() + " " +  fdp.getValue() + " push into database" );
        pushToDataBaseConnector(fdp.getLabel(), Float.toString(fdp.getValue()));
    }

    /**
     * Print and push a BinaryDataPoint on the DataBase
     * @param bdp BinaryDataPoint
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp){
        System.out.println(bdp.getLabel() + " " + bdp.getValue() + " push into database" );
        pushToDataBaseConnector(bdp.getLabel(), Boolean.toString(bdp.getValue()));
    }

    public static void main(String[] args) throws Exception {
    /*  DatabaseConnector dbc = DatabaseConnector.getInstance();
        while (true) {


            FloatDataPoint fdp1 = new FloatDataPoint("BATT_P_FLOAT", false);
            fdp1.setValue(600);
            dbc.onNewValue(fdp1);

            Utility.waitSomeTime(1000);

            FloatDataPoint fdp = new FloatDataPoint("BATT_CHRG_FLOAT", true);
            fdp.setValue(0.5f);
            dbc.onNewValue(fdp);
        }*/


    }
}
