package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;

public class BooleanRegister {

    public BinaryDataPoint dataPoint;
    private int address;

    /* Constructor class BooleanRegister */

    public BooleanRegister(int a, String label, boolean isOutput) {
        address = a;
        dataPoint = new BinaryDataPoint(label, isOutput);
    }

    /* Function to read the value from Modbus */

    public void read() {

        boolean value = ModbusAccessor.getInstance().readBoolean(address);
        try {
            dataPoint.setValue(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*Function to write the value in Modbus */

    public void write() {

        ModbusAccessor.getInstance().writeBoolean(address, dataPoint.getValue());
    }
}
