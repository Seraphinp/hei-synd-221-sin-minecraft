package ch.hevs.isi.field;

import ch.hevs.isi.core.FloatDataPoint;

public class FloatRegister {

    private int address;
    private FloatDataPoint dataPoint;
    private int range;
    private int offset;
    private ModbusAccessor ma = ModbusAccessor.getInstance();

    /* Constructor class FloatRegister */

    public FloatRegister(int a, String label, boolean isOutput, int r, int o){
        address = a;
        dataPoint = new FloatDataPoint(label, isOutput);
        range = r;
        offset = o;


    }

    public void read() {
        try {
            dataPoint.setValue(ma.readFloat(address)*range+offset);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void write(){
        ModbusAccessor.getInstance().writeFloat(address, (dataPoint.getValue()-offset)/range);
    }

    public static void main(String[] args) {
        FloatRegister r1 = new FloatRegister(2000, "toto", false, 1, 0);
        System.out.println(r1.dataPoint.getValue());
        try {
            r1.dataPoint.setValue(2f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(r1.address);
    }
}
