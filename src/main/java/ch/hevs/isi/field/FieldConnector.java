package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class FieldConnector implements DataPointListener {
    private static FieldConnector fc = null;

    public static Map<DataPoint, BooleanRegister> booleanMap = new HashMap<>();
    public static Map<DataPoint, FloatRegister> floatMap = new HashMap<>();

    private int range;
    private int offset;

    private FieldConnector(){
    }

    /**
     * @return FieldConnector
     */
    public static FieldConnector getInstance(){
        if(fc == null){
            fc = new FieldConnector();
        }
        return fc;
    }

    /**
     * Print and push a FloatDataPoint into field
     * @param fdp
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        if(fdp.isOutput()) {
            System.out.println(fdp.getLabel() + " " + fdp.getValue() + " push into field");
        }
    }

    /**
     * Print and push a BinaryDataPoint into field
     * @param bdp
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        if(bdp.isOutput()) {
            System.out.println(bdp.getLabel() + " " + bdp.getValue() + " push into field");
        }
    }


    private void poll() {

        for (BooleanRegister br : booleanMap.values()) {
            br.read();
        }
        for (FloatRegister fr : floatMap.values()) {
            fr.read();
        }
    }


    public void startPollIn(int nTime) {

        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                poll();
            }
        }, 0, nTime);
    }


    public void adBooleanRegister(int address, String label, boolean isOutput) {

        BooleanRegister br = new BooleanRegister(address, label, isOutput);
        DataPoint bdp = BinaryDataPoint.getDataPointFromLabel(label);
        booleanMap.put(bdp, br);
    }


    public void adFloatRegister(int address, String label, boolean isOutput, int offset, int range) {

        this.range = range;
        this.offset = offset;
        FloatRegister fr = new FloatRegister(address, label, isOutput, range, offset);
        DataPoint fdp = FloatDataPoint.getDataPointFromLabel(label);
        floatMap.put(fdp, fr);
    }


    public BooleanRegister getRegisterFromDataPoint(BinaryDataPoint bdp) {

        return booleanMap.get(bdp);
    }


    public FloatRegister getRegisterFromDataPoint(FloatDataPoint fdp) {

        return floatMap.get(fdp);
    }
}

//public static void main(String[] args) throws Exception {
       /*BinaryDataPoint bdp = new BinaryDataPoint("Binary", true);
        bdp.setValue(true);
        BinaryDataPoint bd = new BinaryDataPoint("Usine", true);
        bd.setValue(true);
        }
        */

