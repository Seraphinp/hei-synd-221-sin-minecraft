package ch.hevs.isi.field;
import com.serotonin.modbus4j.*;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.locator.BaseLocator;

public class ModbusAccessor {

    private ModbusMaster _modbus;
    private static ModbusAccessor pInstance = null;

    public static ModbusAccessor getInstance() {

        if (pInstance == null) {

            pInstance = new ModbusAccessor();
        }
        return pInstance;
    }

    private ModbusAccessor() {
        _modbus = null;
        IpParameters params = new IpParameters();
        params.setHost("127.0.0.1");
        params.setPort(1502);
        _modbus = new ModbusFactory().createTcpMaster(params, false);
        try {
            _modbus.init();
        } catch (ModbusInitException e) {
            e.printStackTrace();
        }
    }

    public void connect(String host, Integer port) {
        IpParameters params = new IpParameters();
        params.setHost("127.0.0.1");
        params.setPort(1502);
        _modbus = new ModbusFactory().createTcpMaster(params, false);
        try {
            _modbus.init();
        } catch (ModbusInitException e) {
            e.printStackTrace();
        }
    }

    public Boolean readBoolean(int address) {
        try {
            return _modbus.getValue(BaseLocator.coilStatus(1, address));
        } catch (ModbusTransportException | ErrorResponseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Float readFloat(int address) {
        try {
            return _modbus.getValue(BaseLocator.inputRegister(1, address, DataType.FOUR_BYTE_FLOAT)).floatValue();
        } catch (ModbusTransportException | ErrorResponseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void writeBoolean(int address, boolean value) {

        try {
            _modbus.setValue(BaseLocator.coilStatus(1, address), value);
        } catch (ModbusTransportException | ErrorResponseException e) {
            e.printStackTrace();
        }
    }

    public void writeFloat(int address, float value) {

        try {
            _modbus.setValue(BaseLocator.holdingRegister(1, address, DataType.FOUR_BYTE_FLOAT), value);
        } catch (ModbusTransportException | ErrorResponseException e) {
            e.printStackTrace();
        }
    }

}
