package ch.hevs.isi.web;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.*;
import java.net.InetSocketAddress;
import java.util.Random;
import java.util.Vector;


public class WebConnector extends WebSocketServer implements DataPointListener {
    private static WebConnector wc = null;
    private static Vector<String> v = new Vector<>();

    /** Create a WebSocket
     *
     * @param port port to use
     */
    private WebConnector(int port){
        super(new InetSocketAddress(port));
        this.start();
    }


    public static WebConnector getInstance(){
        if(wc == null){
            wc = new WebConnector(8888);
        }
        return wc;
    }

    @Override
    public void onStart() {

    }

    /** Send the value from the vector at the opening
     *
     * @param webSocket WebSocket
     * @param clientHandshake connection client/server
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        webSocket.send("Welcome you are connected");
        for(int i = 0; i < v.size(); i++){
           webSocket.send(v.elementAt(i));
       }

    }

    /** Close the WebSocket
     *
     * @param webSocket
     * @param i
     * @param s
     * @param b
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
    webSocket.close();

    }

    /** Check if DataPoint is float or boolean and set the value given
     *
     * @param webSocket WebSocket
     * @param s "label=value"
     */
    @Override
    public void onMessage(WebSocket webSocket, String s) {
        if(s.length() >= 3) {
        String[] data = s.split("=");
        String label = data[0];
        String value = data[1];

        if(value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")) {
            BinaryDataPoint bdp = (BinaryDataPoint) DataPoint.getDataPointFromLabel(label);

            try {
                bdp.setValue(Boolean.parseBoolean(value));
                sendResponse(webSocket, bdp.getLabel(), String.valueOf(bdp.getValue()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            FloatDataPoint fdp = (FloatDataPoint) DataPoint.getDataPointFromLabel(label);
            try {
                fdp.setValue(Float.parseFloat(value));
                sendResponse(webSocket, fdp.getLabel(), String.valueOf(fdp.getValue()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        }
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {

    }
    public void sendResponse(WebSocket webSocket, String label, String value){
        webSocket.send(label + "=" + value);
        System.out.println(label + "=" + value);
    }

    @Override
    public void onNewValue(FloatDataPoint fdp) {
        System.out.println(fdp.getLabel() + " " + fdp.getValue() + " push into webpages" );
        v.add(fdp.getLabel() + "=" + fdp.getValue());
    }


    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        System.out.println(bdp.getLabel() + " " + bdp.getValue() + " push into webpages" );
        v.add(bdp.getLabel() + "=" + bdp.getValue());
    }

    /**
     * Read the CSV file and know if the DataPoint is an Output/Input and float/boolean
     */
    public static void readCsv(){
        try {
            BufferedReader CSV = new BufferedReader(new FileReader("C:\\Users\\Séraphin\\Desktop\\Projet_electricalAge\\electricalAge-code\\src\\main\\java\\ch\\hevs\\isi\\web\\ModbusEA_SIn.csv"));
            String data ;
            String[] dataArray;

            for( data = CSV.readLine(); data != null; data = CSV.readLine()){
                dataArray = data.split(";");
                boolean output = dataArray[4].equalsIgnoreCase("Y");

                if(dataArray[0].endsWith("SW")){
                    new BinaryDataPoint(dataArray[0], output);
                    BinaryDataPoint binaryDataPoint = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel(dataArray[0]);
                   // binaryDataPoint.setValue(true);
                }else if(dataArray[0].endsWith("FLOAT") ||  dataArray[0].endsWith("SP") ||  dataArray[0].endsWith("AMOUNT")  || dataArray[0].endsWith("SCORE") || dataArray[0].endsWith("ENERGY")) {
                    new FloatDataPoint(dataArray[0], output);
                    FloatDataPoint floatDataPoint = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel(dataArray[0]);
                   // float f = new Random().nextFloat();
                    //floatDataPoint.setValue(f);
                }
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void main(String[] args) throws Exception {

        readCsv();
    }
        /*new BinaryDataPoint("REMOTE_WIND_SW", true);
        BinaryDataPoint binaryDataPoint = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_WIND_SW");
        binaryDataPoint.setValue(true);

       /*new BinaryDataPoint("SOLAR_CONNECT_SW", false);
        BinaryDataPoint binaryDataPoint3 = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("SOLAR_CONNECT_SW");
        binaryDataPoint3.setValue(true);

       /* new BinaryDataPoint("REMOTE_SOLAR_SW", true);
        BinaryDataPoint binaryDataPoint2 = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
        binaryDataPoint2.setValue(true);

        new BinaryDataPoint("SOLAR_CONNECT_SW", true);
        BinaryDataPoint binaryDataPoint3 = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("SOLAR_CONNECT_SW");
        binaryDataPoint3.setValue(true);

        new FloatDataPoint("REMOTE_COAL_SP", true);
        FloatDataPoint floatDataPoint = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
        floatDataPoint.setValue(0.5f);

        new FloatDataPoint("COAL_SP", true);
        FloatDataPoint floatDataPoint3 = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("COAL_SP");
        floatDataPoint3.setValue(0.5f);

        new FloatDataPoint("REMOTE_FACTORY_SP", false);
        FloatDataPoint floatDataPoint1 = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
        floatDataPoint1.setValue(0.7f);

        new FloatDataPoint("SOLAR_P_FLOAT", false);
        FloatDataPoint floatDataPoint2 = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("SOLAR_P_FLOAT");
        floatDataPoint2.setValue(1000f);

    } */


}
