package ch.hevs.isi.core;

public class FloatDataPoint extends DataPoint{
    private float value;

    /**
     * Create a FloatDataPoint
     * @param label label of DataPoint
     * @param isOutput Input or Output
     */
    public FloatDataPoint(String label, boolean isOutput){
        super(label, isOutput);

    }

    /**
     * Set the FloatDataPoint's value
     * @param value flaot value
     * @throws Exception
     */
    public void setValue(float value) throws Exception {
        this.value = value;
        dbc.onNewValue(this);
        wc.onNewValue(this);
        fc.onNewValue(this);

    }

    /**
     * Get the FloatDataPoint's value
     * @return value
     */
    public float getValue(){
        return value;
    }
}
