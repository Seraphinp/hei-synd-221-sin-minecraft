package ch.hevs.isi.core;

public interface DataPointListener {
     void onNewValue(FloatDataPoint fdp) throws Exception;
     void onNewValue(BinaryDataPoint bdp) throws Exception;

}
