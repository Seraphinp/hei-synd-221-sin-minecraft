package ch.hevs.isi.core;

public class BinaryDataPoint extends DataPoint {

    private boolean value;

    /**
     * Create a BinaryDataPoint
     * @param label label of DataPoint
     * @param isOutput Input or Output
     */
    public BinaryDataPoint(String label, boolean isOutput){

        super(label, isOutput);
    }

    /**
     * Set the BinaryDataPoint's value
     * @param value true or false
     * @throws Exception
     */
    public void setValue(boolean value) throws Exception {
        this.value = value;
        dbc.onNewValue(this);
        wc.onNewValue(this);
        fc.onNewValue(this);

    }

    /**
     * Get the BinaryDataPoint's value
     * @return value
     */
    public boolean getValue(){
        return value;
    }
}
