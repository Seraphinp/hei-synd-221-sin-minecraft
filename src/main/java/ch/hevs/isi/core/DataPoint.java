package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

import java.util.HashMap;

public abstract class DataPoint {
    protected static FieldConnector fc = FieldConnector.getInstance();
    protected static DatabaseConnector dbc = DatabaseConnector.getInstance();
    protected static WebConnector wc = WebConnector.getInstance();
    private static HashMap<String, DataPoint> hm = new HashMap<>();
    private String label;
    private boolean isOutput;

    protected DataPoint(String label, boolean isOutput){
        this.label = label;
        this.isOutput = isOutput;
        hm.put(label, this);
    }

    /**
     * Get a DataPoint from a label
     * @param label label of DataPoint
     * @return DataPoint
     */
    public static DataPoint getDataPointFromLabel(String label){
        return hm.get(label);

    }

    /**
     * Get a label from a DataPoint
     * @return label
     */
    public String getLabel(){
        return label;
    }

    /**
     * Get if the DataPoint is an output
     * @return isOutput
     */
    public boolean isOutput(){
        return isOutput;
    }
}
