
 _____ _           _        _           _    ___             
|  ___| |         | |      (_)         | |  / _ \            
| |__ | | ___  ___| |_ _ __ _  ___ __ _| | / /_\ \ __ _  ___ 
|  __|| |/ _ \/ __| __| '__| |/ __/ _` | | |  _  |/ _` |/ _ \
| |___| |  __/ (__| |_| |  | | (_| (_| | | | | | | (_| |  __/
\____/|_|\___|\___|\__|_|  |_|\___\__,_|_| \_| |_/\__, |\___|
                                                   __/ |     
                                                  |___/      
Auteur: Séraphin Pitteloud
Date: 29.03.2021


Nous avons développé un système de supervision pour des systèmes industriels afin de pouvoir surveiller, optimiser et enregistrer le fonctionnement du système. L'ensemble du projet à été codé entièrement en java.

Nous avons simuler un système via une map de Minecraft Electrical Age. Dans cette map  Minecraft, nous avons les producteurs d’énergie suivants: 
- Eoliennes, panneaux solaires et une usine à charbon
ainsi que les consommateurs suivants:
- usine, bunker, maison et éclairage public
Nous disposons aussi d'une batterie pour stocker de l'énergie.

Le but de ce projet est de pouvoir gérer et optimiser ces différents consommateurs et producteurs de courants pour réussir à garder une tension stable dans tout le système au fil des jours. Nous devons maintenir le système dans une plage de tension définie. 
Nous avions à disposition une page web sur laquelle nous devions pouvoir afficher les variables du système et pouvoir en modifier quelques unes ainsi que Grafana, un outil qui permet de représenter graphiquement les données stockées sur notre base de données sur une échelle de temps.

Structure:
- Core : Gère les DataPoints, float ou booléen
- Field : Assure la liaison entre les DataPoints et le serveur Minecraft
- DataBase : Permet d'écrire et de lire sur la base de données
- Web : Affiche les données sur la page web
- SmartControl : Gère la production éléctrique du système en fonction des divers paramètres

Documentation:
- Notre projet dispose d'une javadoc disponible dans le dossier 'javadoc' afin de fournir une explication sur les différentes classes et méthode employées dans le projet.
- La classe WebConnector contient un fichier .csv qui permet de définir les DataPoints utilisés pour notre client web.

Utilisation:
- Lancer le monde minecraft Electrical Age
- Exécuter le launcher Minecraft.jar avec la commande suivante: java -jar ./Minecraft.jar influx.sdi.hevs.ch SIn15 localhost 1502 -modbus4j
- Ouvrir les pages web: Grafana et le client web
- Contrôler l'affichage des valeurs sur ces pages web ainsi que le bon fonctionnement de l'optimisation réalisée dans le smartcontrol




